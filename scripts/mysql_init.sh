#mysql_init.sh <dbName> <dbUser> <dbPw> <root_pw>

db_name=$1
db_user=$2
pw=$3
root_pw=$4

mysql -u root -p${root_pw} -e "CREATE DATABASE ${db_name}"
mysql -u root -p${root_pw} -e "GRANT ALL PRIVILEGES ON ${db_name}.* TO ${db_user}@localhost IDENTIFIED BY '${pw}'"