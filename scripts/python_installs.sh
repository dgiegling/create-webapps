#python_installs.sh <env_path> <python_version_number> <install_gunicorn> <install_psycopg2> <localize> <install_mysql> <install_south> <django_version>

env_path=$1
python_version=python$2


python3 -m venv ${env_path}
#virtualenv -p /usr/bin/${python_version} ${env_path}
source ${env_path}/bin/activate

ARGS=8

if [ $# -ne "$ARGS" ]
	then
	pip install django
else
	pip install django==$8
fi

pip install django-widget-tweaks
if [ "$3" == "1" ]
	then
	pip install gunicorn
	pip install setproctitle
fi
if [ "$4" == "1" ]
	then
	pip install psycopg2
fi
#localize
if [ "$5" == "1" ]
	then
	pip install django-rosetta
	pip install django-modeltranslation
fi
if [ "$6" == "1" ]
	then
	pip install --allow-all-external mysql-connector-python
fi
if [ "$7" == "1" ]
	then
	pip install south
fi
if [ "$9" == "1" ]
	then
	pip install redis
fi
deactivate