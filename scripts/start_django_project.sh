#start_django_projects.sh <project_name> <app_name> <env_path>

project_name=$1
app_name=$2
env_path=$3


source ${env_path}/bin/activate
cd ${env_path}
django-admin.py startproject ${project_name}
project_path=${env_path}/${project_name}
cd ${project_path}
python manage.py startapp ${app_name}

mkdir static
mkdir static/css
mkdir static/js
touch static/favicon.ico
touch static/robots.txt

#touch ${app_name}/urls.py -> template
mkdir ${app_name}/templates
mkdir ${app_name}/templates/${app_name}
mkdir ${app_name}/templatetags
touch ${app_name}/templatetags/__init__.py
touch ${app_name}/templatetags/costum_tags.py
#touch ${project_name}/devel_settings.py -> template
#touch ${project_name}/local_settings.py -> template
#touch ${project_name}/production_settings.py -> template