#postgres_init.sh <db_name> <db_user>

db_name=$1
db_user=$2

su - postgres -c "createuser -P ${db_user}"
su - postgres -c "createdb --owner ${db_user} ${db_name}"
#su - postgres -c "psql -c \"GRANT ALL PRIVILEGES ON DATABASE ${db_name} TO ${db_user};\""