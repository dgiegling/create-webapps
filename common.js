var readline   = require( 'readline' )
var format     = require( 'util' ).format
var fs         = require( 'fs' )
var exec       = require( 'child_process' ).exec
var execSync   = require( 'child_process' ).execSync
var path       = require( 'path' )


var rl = readline.createInterface( {
    input: process.stdin,
    output: process.stdout
} )

var closeRl = function(){
	rl.close()
}

var askFor = function( name, defaultValue, dependsOn, clean, validator, callback ){

	if( dependsOn && dependsOn.filter( function( val ){ return !val } ).length ){
		callback( undefined )
	}
	else {
		var defaultString = defaultValue !== undefined ? format( ' (default %s)', defaultValue ) : '';

		( function doQuestion(){
			rl.question( format( '%s%s:', name, defaultString ), function ( response ) {
				response = response.trim()
				var value = clean( response )
				var hasDefaultValue = defaultValue !== undefined
				
				if( ( response == '' && !hasDefaultValue ) || ( response != '' && !validator( value ) ) ){
					console.log( 'Required or invalid! Try again!' )
					doQuestion()
				}
				else
					callback( validator( value ) ? value : clean( defaultValue ) )
			})
		} )();

	}
}

var askForInteger = function (name, defaultValue, dependsOn, callback) {
    askFor( name, 
    	defaultValue, 
    	dependsOn,
    	parseInt, 
    	function( s ){ return !isNaN( s ) }, 
    	callback )
}

var askForString = function (name, defaultValue, dependsOn, callback) {
	askFor( name, 
		defaultValue, 
		dependsOn,
		function( s ){ return s.trim() }, 
		function( s ){ return s != '' }, 
		callback )
}

var askForDomain = function (name, defaultValue, dependsOn, callback) {
	askForRegexString( name, 
		defaultValue, 
		/^([A-Za-z0-9-]{1,63}\.)+[A-Za-z]{2,6}$/, 
		dependsOn,
		callback )
}

var askForDirectory = function(name, defaultValue, dependsOn, callback){
	askFor( name,
		defaultValue,
		dependsOn,
		function( s ){ return s.trim() },
		function( s ){ return fs.existsSync( s ) },
		callback )
}

var askForRegexString = function (name, defaultValue, regex, dependsOn, callback) {
	askFor( name, 
		defaultValue, 
		dependsOn,
		function( s ){ return s.trim(); }, 
		function( s ){ return regex.test( s ); }, 
		callback )
}

var askForRegexStringList = function (name, defaultValue, regex, dependsOn, callback) {
	askFor( name, 
		defaultValue, 
		dependsOn,
		function( s ){ 
			var s = s.split( ',' ), r = []

			for( var i = 0; i < s.length; ++i )
				r.push( s[ i ].trim() )

			return r
		}, 
		function( s ){ 
			return s.filter( function( s ){ 
				return !regex.test( s ) 
			} ).length == 0 
		}, 
		callback )
}

var askForChoice = function (name, defaultValue, choices, dependsOn, callback) {
	askFor( format( '%s (choices: %s)', name, choices.join( ', ' ) ),
		defaultValue,
		dependsOn,
		function( s ){ return s.trim() }, 
		function( s ){ return choices.indexOf( s ) != -1 }, 
		callback )
}

var askForBool = function (name, defaultValue, dependsOn, callback) {
	askFor( name,
		defaultValue,
		dependsOn,
		function( s ){ return s.trim() == '' ? undefined : s.trim() === 'y'; }, 
		function( s ){ return s === true || s === false; }, 
		callback )
}

var execCmd = function( command, callback, onError ){
	exec( command, { shell: '/bin/bash' }, function( error, stdout, stderr ){
		console.log( stdout )
		console.log( stderr )
		
		if (error !== null) {
			//console.log('exec error: ' + error)
			onError && onError( error )
		}
		else{
			callback && callback()
		}
	} )
}

var execCmdSync = function( command, onError ){
	try{
		var out = execSync( command, { shell: '/bin/bash' } )
		console.log( out )
	}
	catch( err ){
		console.log( err )
		onError && onError( err )
		return err
	}
	return out
}

var execCmds = function( cmds, onFinish, onError, onEach ){

	if( cmds.length == 0 ){
		onFinish && onFinish()
	}
	else{

		var c = 0
		for( var i = 0; i < cmds.length; ++i ){
			execCmd( cmds[ i ], function(){
				onEach && onEach()
				if( ++c == cmds.length && onFinish )
					onFinish()
			}, onError )
		}
	}
}

var execCmdsSync = function( cmds, onError, onEach ){

	var res = []

	for( var i = 0; i < cmds.length; ++i ){
		onEach && onEach()
		res.push( execCmdSync( cmds[ i ], onError ) )
	}

	return res
}


var renderAndSaveTemplate = function( name, extension, dir, args, newFilename ){
	require( 'jinjs' ).registerExtension( extension )
	var context = { args: args }
	var rendered = require( format( '%s/templates/%s', __dirname, name ) ).render( context )
	fs.writeFileSync( path.join( dir, newFilename || format( '%s%s', name, extension ) ), rendered )
}

var changeBackToDirname = function(){
	process.chdir( __dirname )
}

module.exports = {
	askForBool           : askForBool,
	askForChoice         : askForChoice,
	askForDirectory      : askForDirectory,
	askForDomain         : askForDomain,
	askForString         : askForString,
	askForRegexString    : askForRegexString,
	askForInteger        : askForInteger,
	askForRegexStringList: askForRegexStringList,
	execCmd              : execCmd,
	execCmdSync          : execCmdSync,
	execCmds             : execCmds,
	execCmdsSync         : execCmdsSync,
	renderAndSaveTemplate: renderAndSaveTemplate,
	changeBackToDirname  : changeBackToDirname,
	closeRl              : closeRl,
}