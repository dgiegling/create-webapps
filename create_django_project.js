'use strict';

/*
args:
	projectName: <string (only letters)>
	domain: <url>
	develdomain: <url>
	appName: <string (only letters)>
	pythonVersion: <version number (for example 2.7)>
	productionServer: <y/n>
	installdb: <y/n>
	databaseBackend: <mysql, sqllite or postgresql> (depends on installdb=y)
	usegit: <y/n>
	gitprotocol: <ssh or https> (depends on usegit=y)
	giturl: <url> (depends on usegit=y)
	mysqlrootpw: <string> (depends on database=mysql, installdb=y)
	installServerScripts: <y/n>
	serverType: <devel, apache, nginx> (depends installServerScripts=y)
*/

//var readline   = require( 'readline' )
var format     = require( 'util' ).format
var fs         = require( 'fs' )
//var exec       = require( 'child_process' ).exec
//var execSync   = require( 'child_process' ).execSync
//var suppose    = require( './suppose/suppose' )
var pwGen      = require( 'password-generator' )
var path       = require( 'path' )
var common     = require( './common' )

var execCmdSync = common.execCmdSync
var execCmd = common.execCmd
var renderAndSaveTemplate = common.renderAndSaveTemplate
var changeBackToDirname = common.changeBackToDirname




var init = function( args ){

	console.log( 'starting init' )
	console.log( args )

	var initAppUser = function( callback ){
		if( args.isProductionServer ){
			try{
				var command = format(
					'useradd --system --gid %s --shell /bin/bash --home %s %s',
					args.systemGroupName,
					args.envPath,
					args.systemUserName
				)

				console.log( 'creating app user...' )
				execCmd( command, function(){
					console.log( 'created app user!' )
					callback && callback()
				}, callback )
			} catch( e ){
				console.log( format( 'app user creation failed: %s', e ) )
				callback && callback()
			}
		}
		else{
			console.log( 'skipping app user creation...' )
			callback && callback()
		}
	}

	var initDb = function( callback ){
		
		var allSqlLite = ( args.isProductionServer && args.databaseBackendProduction == 'sqllite' ) ||
			( !args.isProductionServer && args.databaseBackendDevel == 'sqllite' )

		if( !args.installDb || allSqlLite ){
			console.log( 'skipping database initializing...' )
			callback && callback()
		}
		else{
			console.log( 'installing database...' )

			var mysqlProduction = args.isProductionServer && args.databaseBackendProduction == 'mysql'
			var mysqlDevel = !args.isProductionServer && args.databaseBackendDevel == 'mysql'
			var postgreProduction = args.isProductionServer && args.databaseBackendProduction == 'postgresql'
			var postgreDevel = !args.isProductionServer && args.databaseBackendDevel == 'postgresql'
			
			if( mysqlProduction || mysqlDevel ){
				// sudo apt-get install mysql-server
				require( './mysql' ).mysqlInitSync( args )
				console.log( format( 'added mysql db "%s"', args.dbName ) )
				console.log( format( 'added mysql user "%s"', args.dbUser ) )
				console.log( format( 'pw is %s', args.dbPw ) )
			}
			else if( postgreDevel || postgreProduction ){

				require( './postgres' ).postgresInit( args, callback )
				
			}
			else{
				callback && callback()
			}
		}
	}

	var asyncInstalls = function( callback ){

		if( args.programmingEnv.indexOf( 'async' ) == -1 ){
			callback && callback()
		}

		else{
			execCmdSync( format( 'mkdir -p %s', args.projectDirAsync ) )
			execCmdSync( format( 'mkdir -p %s', path.join( args.projectDir, args.appName, 'async' ) ) )
			console.log( 'installing async environment and modules...' )
			require( './async_installs' ).asyncInstalls( args, callback )
		}
	}
	
	// python_installs.sh <project_name> <python_version_number>
	var pythonInstalls = function( callback ){

		if( args.programmingEnv.indexOf( 'django' ) == -1 ){
			callback && callback()
		}

		else{
			console.log( 'installing virtual environment and python packages...' )
			//if( parseFloat( args.pythonVersion ) >= 3.6 )
			require( './django_installs' ).djangoInstallsSync36( args )
			//else if( parseFloat( args.pythonVersion ) > 3.4 )
			//	require( './django_installs' ).djangoInstallsSync( args )
			//else
			//	require( './django_installs' ).djangoInstallsSync34( args )
			callback && callback()
		}
	}

	// git clone <existing_project> or start_django_project.sh <project_name> <app_name>
	//   -settings.py
	//   -devel_settings.py
	//   -production_settings.py
	//   -local_settings.py -> also at git clone
	//   -urls.py
	var startDjangoProject = function( callback ){

		execCmdSync( format( 'mkdir -p %s', args.staticRoot ) )
		execCmdSync( format( 'mkdir -p %s', args.mediaRoot ) )

		if( args.programmingEnv.indexOf( 'django' ) == -1 ){
			callback && callback()
		}

		else if( args.useExistingProject && !args.cloneExistingProject ){
			try{
				renderAndSaveTemplate( 
					'local_settings', '.py', 
					path.join( args.projectDir, args.projectName ), args )
				callback && callback()
			}
			catch(e){
				console.log( format( 'cloning project failed: %s', e ) )
				callback && callback()
			}

		}

		else if( args.useExistingProject ){
			console.log( 'cloning existing project...' )
			try{
				process.chdir( args.envPath )
				var command = format( 'git clone %s', args.gitRemoteUrl )
				execCmd( command, function(){ 
						
						renderAndSaveTemplate( 
							'local_settings', '.py', 
							path.join( args.projectName, args.projectName ), args )
						changeBackToDirname()
						callback && callback() 

					}, changeBackToDirname )
			}
			catch( e ){
				console.log( format( 'cloning project failed: %s', e ) )
				callback && callback()
			}
		}
		else{
			console.log( 'starting new django project...' )
			
			require( './start_django_project' ).startDjangoProjectSync( args )

			renderAndSaveTemplate( 
				'urls_project', '.py', 
				path.join( args.projectDir, args.projectName ), args, 'urls.py' )
			renderAndSaveTemplate( 
				'urls_app', '.py', 
				path.join( args.projectDir, args.appName ), args, 'urls.py' )
			renderAndSaveTemplate( 
				'settings', '.py', 
				path.join( args.projectDir, args.projectName ), args )
			renderAndSaveTemplate( 
				'production_settings', '.py', 
				path.join( args.projectDir, args.projectName ), args )
			renderAndSaveTemplate( 
				'local_settings', '.py', 
				path.join( args.projectDir, args.projectName ),args )
			renderAndSaveTemplate( 
				'devel_settings', '.py', 
				path.join( args.projectDir, args.projectName ), args )
			if( args.localize )
				renderAndSaveTemplate( 
					'translation', '.py', 
					path.join( args.projectDir, args.appName ),args )
			
			if( args.localize ){

				for( var i = 0; i < args.locales.length; ++i ){
					var locale = args.locales[ i ]
					var dir = path.join( args.projectDir, args.appName, 'locale', locale )
					execCmdSync( format( 'mkdir -p %s', dir ) )
				}
			}

			callback && callback()
		}
	}

	var startAsyncProject = function( callback ){

		if( args.programmingEnv.indexOf( 'async' ) == -1 ){
			callback && callback()
		}

		else if( ( args.useExistingProject && !args.cloneExistingProject ) || 
			( args.cloneExistingProject && args.programmingEnv.indexOf( 'django' ) != -1 ) ){
			renderAndSaveTemplate( 'async/local_settings', '.js', args.projectDirAsync, args, 'local_settings.js' )
			callback && callback()
		}

		else if( args.useExistingProject ){
			console.log( 'cloning existing project...' )
			process.chdir( args.envPath )
			var command = format( 'git clone %s', args.gitRemoteUrl )
			execCmd( command, function(){ 
					
					renderAndSaveTemplate( 'async/local_settings', '.js', args.projectDirAsync, args, 'local_settings.js' )
					changeBackToDirname()
					callback && callback() 

				}, changeBackToDirname )
		}
		else{
			console.log( 'starting new async project...' )
			
			renderAndSaveTemplate( 'async/server', '.js', args.projectDirAsync, args, args.entryPointAsync )
			renderAndSaveTemplate( 'async/settings', '.js', args.projectDirAsync, args, 'settings.js' )
			renderAndSaveTemplate( 'async/production_settings', '.js', args.projectDirAsync, args, 'production_settings.js' )
			renderAndSaveTemplate( 'async/local_settings', '.js', args.projectDirAsync, args, 'local_settings.js' )
			renderAndSaveTemplate( 'async/devel_settings', '.js', args.projectDirAsync, args, 'devel_settings.js' )

			callback && callback()
		}
	}

	
	// git_init.sh <git_url>
	//   -.gitignore
	var initGit = function( callback ){
		if( !args.addGit ){
			console.log( 'skipping git init...' )
			callback && callback();
		}
		else{
			console.log( 'initializing git...' )
			process.chdir( path.join( args.projectDir ) )

			execCmd( 'git init', function(){
				var command = format( 'git remote add origin %s', args.gitRemoteUrl)
				
				execCmd( command, function(){

					renderAndSaveTemplate( 
						'.gitignore', '.txt', 
						path.join( args.projectDir ), args, '.gitignore' )

					console.log( 'initialized git and added origin.' )
					changeBackToDirname()
					callback && callback()

				}, changeBackToDirname )
			
			}, changeBackToDirname ) 
		}
	}
	// create helper scripts
	//   -update.sh
	//   -commit.sh
	//   -migrate.sh
	//   -translate.sh (if different locales)
	//   -rundevelserver.sh
	//   -pip_install.sh
	//   -TODO backupjob
	var createHelperScripts = function( callback ){
		renderAndSaveTemplate( 'update', '.sh', path.join( args.projectDir ), args )
		renderAndSaveTemplate( 'commit', '.sh', path.join( args.projectDir ), args )
		
		if( args.programmingEnv.indexOf( 'django' ) != -1 ){
			renderAndSaveTemplate( 'pip_install', '.sh', path.join( args.projectDir ), args )
			renderAndSaveTemplate( 'develrun', '.sh', path.join( args.projectDir ), args )
			renderAndSaveTemplate( 'migrate', '.sh', path.join( args.projectDir ), args )

			if( args.localize )
				renderAndSaveTemplate( 'translate', '.sh', path.join( args.projectDir ), args )
		}

		if( args.programmingEnv.indexOf( 'async' ) != -1 ){
			renderAndSaveTemplate( 'async/runasync', '.bash', path.join( args.projectDir ), args, 'runasync.bash' )
			renderAndSaveTemplate( 'async/runasync', '.bash', path.join( args.envPath, 'bin' ), args, 'async_start' )
			renderAndSaveTemplate( 'async/async_install', '.bash', path.join( args.projectDir ), args, 'async_install.bash' )
		}
		
		callback && callback()
	}

	// create server files
	//   -secretkey file
	//   -wsgi app file (apache)
	//   -siteavailable (apache) -> a2ensite
	//   -nginx files
	//   -media & static root dirs
	var createServerFiles = function( callback ){
		if( !args.installServerScripts ){
			console.log( 'skipping server script creation...' )
			callback && callback();
		}
		else{
			
			execCmdSync( format( 'mkdir -p %s', path.join( '/etc', 'webapps' ) ) )

			fs.writeFileSync( args.dbPwFilename, args.dbPw )
			fs.writeFileSync( args.secretKeyFilename, args.secretKeyProduction )

			if( args.serverType == 'apache-wsgi' ){
				console.log( 'installing apache mod-wsgi server scripts...' )

				execCmdSync( format( 'mkdir -p %s', args.wsgiPath ) )
				execCmdSync( format( 'mkdir -p %s', path.join( args.logDir ) ) )

				renderAndSaveTemplate( 'apache2', '.wsgi', args.wsgiPath, args, args.wsgiFilename )
				var newFilename = format( '%s.conf', args.projectName )
				var apacheConfPath = path.join( '/etc', 'apache2', 'sites-available' )
				renderAndSaveTemplate( 'apache2', '.conf', apacheConfPath, args, newFilename )
				process.chdir( apacheConfPath )
				
				var command = format( 'a2ensite %s', newFilename )
				execCmd( command, function(){
					execCmd( 'apache2ctl restart', function(){
						
						changeBackToDirname()
						callback && callback()
					
					}, changeBackToDirname )
				}, changeBackToDirname )
			}
			else if( args.serverType == 'nginx-daphne' ){
				console.log( 'installing nginx daphne server scripts...' )

				execCmdSync( format( 'mkdir -p %s', path.join( args.logDir ) ) )
				execCmdSync( format( 'mkdir -p %s', path.join( args.runDir ) ) )

				if( args.programmingEnv.indexOf( 'django' ) != -1 ){

					if( fs.existsSync( path.join( args.wsgiPath, 'asgi.py' ) ) )
						fs.renameSync( path.join( args.wsgiPath, 'asgi.py' ), 
							path.join( args.wsgiPath, args.wsgiFilename ) )

					renderAndSaveTemplate( 
						'daphne_start', '.bash', 
						path.join( args.envPath, 'bin' ), args, 'daphne_start' )
					var confName = format( '%s_django.conf', args.projectName )
					renderAndSaveTemplate( 
						'supervisor_daphne', '.conf', 
						path.join( '/etc', 'supervisor', 'conf.d' ), args, confName )

					var fd = fs.openSync( 
						path.join( args.logDir, 'daphne_supervisor.log' ), 'w' )
					fs.closeSync( fd )
				}

				if( args.programmingEnv.indexOf( 'async' ) != -1 ){

					var confName = format( '%s_async.conf', args.projectName )
					renderAndSaveTemplate( 
						'supervisor_async', '.conf', 
						path.join( '/etc', 'supervisor', 'conf.d' ), args, confName )
				}


				execCmd( 'supervisorctl reread', function(){
					execCmd( 'supervisorctl update', function(){

						if( args.programmingEnv.indexOf( 'django' ) != -1 ){
							console.log( 'daphne job is running!' )
							var nginxConfName = format( '%s_django.conf', args.projectName )
							
							renderAndSaveTemplate( 
								'nginx_daphne', '.conf',
								path.join( '/etc', 'nginx', 'sites-available' ), args, nginxConfName )

							var enabledPath = path.join( '/etc', 'nginx', 'sites-enabled', nginxConfName )
							if( !fs.existsSync( enabledPath ) )
								fs.symlinkSync( path.join( '/etc', 'nginx', 'sites-available', nginxConfName ),
									enabledPath )
						}

						if( args.programmingEnv.indexOf( 'async' ) != -1 ){
							var nginxConfName = format( '%s_async.conf', args.projectName )
							
							renderAndSaveTemplate( 
								'nginx_async', '.conf',
								path.join( '/etc', 'nginx', 'sites-available' ), args, nginxConfName )

							var enabledPath = path.join( '/etc', 'nginx', 'sites-enabled', nginxConfName )
							if( !fs.existsSync( enabledPath ) )
								fs.symlinkSync( path.join( '/etc', 'nginx', 'sites-available', nginxConfName ),
									enabledPath )
						}
						
						var defaultNginxModule = path.join( '/etc', 'nginx', 'sites-enabled', 'default' );
						if( fs.existsSync( defaultNginxModule ) )
							fs.unlink( defaultNginxModule )

						console.log( 'configured nginx!' )

						execCmd( 'systemctl restart nginx', function(){
							console.log( 'nginx site enabled and restarted!' )
							callback && callback()
						} )
					} )
				} )

			}
			else if( args.serverType == 'nginx-gunicorn' ){
				console.log( 'installing nginx gunicorn server scripts...' )

				execCmdSync( format( 'mkdir -p %s', path.join( args.logDir ) ) )
				execCmdSync( format( 'mkdir -p %s', path.join( args.runDir ) ) )

				if( args.programmingEnv.indexOf( 'django' ) != -1 ){

					if( fs.existsSync( path.join( args.wsgiPath, 'wsgi.py' ) ) )
						fs.renameSync( path.join( args.wsgiPath, 'wsgi.py' ), 
							path.join( args.wsgiPath, args.wsgiFilename ) )

					renderAndSaveTemplate( 
						'gunicorn_start', '.bash', 
						path.join( args.envPath, 'bin' ), args, 'gunicorn_start' )
					var confName = format( '%s_django.conf', args.projectName )
					renderAndSaveTemplate( 
						'supervisor_gunicorn', '.conf', 
						path.join( '/etc', 'supervisor', 'conf.d' ), args, confName )

					var fd = fs.openSync( 
						path.join( args.logDir, 'gunicorn_supervisor.log' ), 'w' )
					fs.closeSync( fd )
				}

				if( args.programmingEnv.indexOf( 'async' ) != -1 ){

					var confName = format( '%s_async.conf', args.projectName )
					renderAndSaveTemplate( 
						'supervisor_async', '.conf', 
						path.join( '/etc', 'supervisor', 'conf.d' ), args, confName )
				}


				execCmd( 'supervisorctl reread', function(){
					execCmd( 'supervisorctl update', function(){

						if( args.programmingEnv.indexOf( 'django' ) != -1 ){
							console.log( 'gunicorn job is running!' )
							var nginxConfName = format( '%s_django.conf', args.projectName )
							
							execCmdSync( 'mkdir -p /etc/nginx/sites-available' )
							execCmdSync( 'mkdir -p /etc/nginx/sites-enabled' )

							renderAndSaveTemplate( 
								'nginx_gunicorn', '.conf',
								path.join( '/etc', 'nginx', 'sites-available' ), args, nginxConfName )

							var enabledPath = path.join( '/etc', 'nginx', 'sites-enabled', nginxConfName )
							if( !fs.existsSync( enabledPath ) )
								fs.symlinkSync( path.join( '/etc', 'nginx', 'sites-available', nginxConfName ),
									enabledPath )
						}

						if( args.programmingEnv.indexOf( 'async' ) != -1 ){
							var nginxConfName = format( '%s_async.conf', args.projectName )
							
							renderAndSaveTemplate( 
								'nginx_async', '.conf',
								path.join( '/etc', 'nginx', 'sites-available' ), args, nginxConfName )

							var enabledPath = path.join( '/etc', 'nginx', 'sites-enabled', nginxConfName )
							if( !fs.existsSync( enabledPath ) )
								fs.symlinkSync( path.join( '/etc', 'nginx', 'sites-available', nginxConfName ),
									enabledPath )
						}
						
						var defaultNginxModule = path.join( '/etc', 'nginx', 'sites-enabled', 'default' );
						if( fs.existsSync( defaultNginxModule ) )
							fs.unlink( defaultNginxModule )

						console.log( 'configured nginx!' )

						execCmd( 'service nginx restart', function(){
							console.log( 'nginx site enabled and restarted!' )
							callback && callback()
						} )
					} )
				} )

			}
		}
	}

	var initPermissions = function( callback ){
		
		console.log( 'initializing user permissions!' )

		if( args.isProductionServer ){

			execCmdSync( format( 
				'chown %s:%s %s', 
				args.systemUserName, 
				args.systemGroupName, 
				args.secretKeyFilename ) )
			execCmdSync( format( 'chmod u+r,og-rwx %s', args.secretKeyFilename ) )
			execCmdSync( format( 
				'chown %s:%s %s', 
				args.systemUserName, 
				args.systemGroupName, 
				args.dbPwFilename ) )
			execCmdSync( format( 'chmod u+r,og-rwx %s', args.dbPwFilename ) )
		}	
		
		execCmdSync( format( 'chown -R %s:%s %s', args.systemUserName, args.systemGroupName, args.envPath ) )
		execCmdSync( format( 'chmod -R u+rw,g-wx,o-rwx %s', args.envPath ) )
		execCmdSync( format( 'chmod u+x,o-rwx,g-wx %s/*.sh', args.projectDir ) )
		execCmdSync( format( 'chmod u+x,o-rwx,g-wx %s/*.bash', args.projectDir ) )

		if( args.isProductionServer ){

			var filesPermissions = 'gu+rwx,o-w,o+rx'

			if( args.serverType == 'nginx-gunicorn' ){
				execCmdSync( format( 'chown -R %s:%s %s', args.systemUserName, args.systemGroupName, args.staticRoot ) )
				execCmdSync( format( 'chown -R %s:%s %s', args.systemUserName, args.systemGroupName, args.mediaRoot ) )
				execCmdSync( format( 'chmod -R %s %s', filesPermissions, args.staticRoot ) )
				execCmdSync( format( 'chmod -R %s %s', filesPermissions, args.mediaRoot ) )

				if( args.programmingEnv.indexOf( 'django' ) != -1 )
					execCmdSync( format( 'chmod ug+rx,o-xwr %s', path.join( args.envPath, 'bin', 'gunicorn_start' ) ) )

				if( args.programmingEnv.indexOf( 'async' ) != -1 )
					execCmdSync( format( 'chmod ug+rx,o-xwr %s', path.join( args.envPath, 'bin', 'async_start' ) ) )


			}
			if( args.serverType == 'nginx-daphne' ){
				execCmdSync( format( 'chown -R %s:%s %s', args.systemUserName, args.systemGroupName, args.staticRoot ) )
				execCmdSync( format( 'chown -R %s:%s %s', args.systemUserName, args.systemGroupName, args.mediaRoot ) )
				execCmdSync( format( 'chmod -R %s %s', filesPermissions, args.staticRoot ) )
				execCmdSync( format( 'chmod -R %s %s', filesPermissions, args.mediaRoot ) )

				if( args.programmingEnv.indexOf( 'django' ) != -1 )
					execCmdSync( format( 'chmod ug+rx,o-xwr %s', path.join( args.envPath, 'bin', 'daphne_start' ) ) )

				if( args.programmingEnv.indexOf( 'async' ) != -1 )
					execCmdSync( format( 'chmod ug+rx,o-xwr %s', path.join( args.envPath, 'bin', 'async_start' ) ) )


			}
			else if( args.serverType == 'apache-wsgi' ){
				//execCmdSync( format( 'usermod -a -G www-data %s', args.systemUserName ) )
				execCmdSync( format( 'chown -R www-data:%s %s', args.systemGroupName, args.staticRoot ) )
				execCmdSync( format( 'chown -R www-data:%s %s', args.systemGroupName, args.mediaRoot ) )
				execCmdSync( format( 'chmod -R %s %s', filesPermissions, args.staticRoot ) )
				execCmdSync( format( 'chmod -R %s %s', filesPermissions, args.mediaRoot ) )

				execCmdSync( format( 
					'chown %s:%s %s', 
					'www-data', 
					'www-data', 
					path.join( args.wsgiPath, args.wsgiFilename ) ) )
				execCmdSync( format( 'chmod gu+r,o-rxw %s', path.join( args.wsgiPath, args.wsgiFilename ) ) )
			}

		}
		callback && callback()
	}

	initAppUser(         function(){
	initDb(              function(){
	pythonInstalls(      function(){
	startDjangoProject(  function(){
	asyncInstalls(       function(){
	startAsyncProject(   function(){
	initGit(             function(){
	createHelperScripts( function(){
	createServerFiles(   function(){
	initPermissions(     function(){
		
		console.log( 'finished creation of new project!' )
		//process.chdir( '..' )

	});});});});});});});});});});
};


common.askForChoice( 
	'Programming environment', 
	'django', 
	[ 'django', 'async', 'django-async' ], 
	undefined, 
	function( programmingEnv ){
common.askForBool( 
	'Use existing project', 
	'n', 
	undefined,
	function( useExistingProject ){
common.askForBool( 
	'Clone existing project', 
	'y',
	[ useExistingProject ],
	function( cloneExistingProject ){
common.askForDirectory( 
	'Install directory (relative or absolute)', 
	'..', 
	undefined, 
	function( installDir ){
common.askForRegexString( 
	'Project name (only letters and underscore)', 
	undefined, 
	/^[a-zA-z_]+$/, 
	undefined, 
	function( projectName ){
common.askForRegexString( 
	'App name (only letters and underscore)', 
	'main', 
	/^[a-zA-z_]+$/, 
	undefined, 
	function( appName ) {
common.askForDomain( 
	'Domain', 
	format( '%s.de', projectName ), 
	undefined, 
	function( domain ){
common.askForDomain( 
	'Devel domain', 
	format( 'test.%s.de', projectName ), 
	undefined, 
	function( develDomain ){
common.askForRegexString( 
	'Python version number (i.e. 3.4)', 
	'3.4', 
	/^[0-9]{1,2}\.[0-9]{1,2}(\.[0-9]{1,2})?/, 
	[ programmingEnv.indexOf( 'django' ) != -1 ], 
	function( pythonVersionNumber ){
common.askForRegexString( 
	'Django version number (i.e. 1.7, leave blank for latest stable)', 
	'', 
	/^[0-9]{1,2}\.[0-9]{1,2}(\.[0-9]{1,2})?/, 
	[ programmingEnv.indexOf( 'django' ) != -1 ], 
	function( djangoVersionNumber ){
common.askForBool( 
	'Install database', 
	'y', 
	undefined, 
	function( installDb ){
common.askForChoice( 
	'Database backend production server', 
	'postgresql', 
	[ 'mysql', 'postgresql', 'sqllite', 'redis' ], 
	[ installDb || useExistingProject ], 
	function( databaseBackendProduction ){
common.askForChoice( 
	'Database backend devel server', 
	'postgresql', 
	[ 'mysql', 'postgresql', 'sqllite', 'redis' ], 
	[ useExistingProject || installDb ], 
	function( databaseBackendDevel ){
common.askForString(
	'Database user',
	undefined,
	[ !installDb, databaseBackendProduction != 'sqllite' || databaseBackendDevel != 'sqllite' ],
	function( dbUser ){
common.askForString(
	'Database name',
	undefined,
	[ !installDb, databaseBackendProduction != 'sqllite' || databaseBackendDevel != 'sqllite' ],
	function( dbName ){
common.askForString(
	'Database password',
	undefined,
	[ !installDb, databaseBackendProduction != 'sqllite' || databaseBackendDevel != 'sqllite' ],
	function( dbPw ){
common.askForString( 
	'Mysql root pw', 
	'', 
	[ installDb, databaseBackendProduction == 'mysql' || databaseBackendDevel == 'mysql' ], 
	function( mysqlRootPw ){
common.askForBool( 
	'Add git repository', 
	'n', 
	[ !useExistingProject ],
	function( addGit ){
common.askForString( 
	'Git remote url', 
	format( 'https://dgiegling@bitbucket.org/dgiegling/%s.git', projectName ),
	[ addGit || ( useExistingProject && cloneExistingProject ) ],
	function( gitRemoteUrl ){
common.askForBool( 
	'Localize', 
	'n', 
	[ programmingEnv.indexOf( 'django' ) != -1 ],
	function( localize ){
common.askForRegexStringList( 
	'Locales (comma separated list)', 
	'de,en', 
	/^[a-z][a-z]$/,
	[ localize, !useExistingProject, programmingEnv.indexOf( 'django' ) != -1 ],
	function( locales ){
common.askForRegexString( 
	'Standard locale', 
	'de',
	/^[a-z][a-z]$/,
	[ localize, !useExistingProject, programmingEnv.indexOf( 'django' ) != -1 ],
	function( standardLocale ){
common.askForChoice( 
	'Server type', 
	'devel',
	[ 'devel', 'apache-wsgi', 'nginx-gunicorn', 'nginx-daphne' ],
	undefined,
	function( serverType ){
common.askForInteger(
	'Server port',
	80,
	[ serverType != 'devel', programmingEnv.indexOf( 'django' ) != -1 ],
	function( serverPort ){
common.askForInteger(
	'Server port async',
	8080,
	[ programmingEnv.indexOf( 'async' ) != -1 ],
	function( serverPortAsync ){
common.askForRegexString( 
	'Async entry point', 
	'server.js', 
	/^[a-zA-z_\-\.0-9]+$/, 
	[ programmingEnv.indexOf( 'async' ) != -1 ], 
	function( entryPointAsync ){
common.askForDirectory( 
	'Location of binary for async js', 
	'/home/<user>/.nvm/versions/node/v0.12.0/bin/node', 
	[ programmingEnv.indexOf( 'async' ) != -1 ], 
	function( binAsync ){
common.askForDomain( 
	'Domain for async server', 
	format( 'async.%s', domain ),
	[ programmingEnv.indexOf( 'async' ) != -1 ], 
	function( domainAsync ){
common.askForString( 
	'System user name', 
	undefined,
	[ serverType == 'devel' ],
	function( systemUserName ){
common.askForString( 
	'System group name', 
	systemUserName,
	[ serverType == 'devel' ],
	function( systemGroupName ){

	common.closeRl()

	installDir = path.resolve( installDir )

	var projectDir, projectDirAsync, envPath
	if( programmingEnv == 'django' ){
		envPath = path.join( installDir, format( '%s_environment', projectName ) )
		projectDir = path.join( envPath, projectName )
	}
	else if( programmingEnv == 'async' ){
		envPath = path.join( installDir, projectName )
		projectDirAsync = envPath
		projectDir = envPath
		
	}
	else if( programmingEnv == 'django-async' ){
		envPath = path.join( installDir, format( '%s_environment', projectName ) )
		projectDir = path.join( envPath, projectName )
		projectDirAsync = path.join( projectDir, projectName, 'async' )
		
	}

	execCmdSync( format( 'mkdir -p %s', envPath ) )

	init( {
		programmingEnv      : programmingEnv,
		projectDir          : projectDir,
		staticRoot          : path.join( '/var', 'www', format( '%s.static', projectName ) ),
		mediaRoot           : path.join( '/var', 'www', format( '%s.media', projectName ) ),
		projectName         : projectName,
		appName             : appName,
		pythonVersionNumber : pythonVersionNumber,
		installDb           : installDb,
		databaseBackendProduction: databaseBackendProduction,
		databaseBackendDevel: databaseBackendDevel,
		domain              : domain,
		isProductionServer  : serverType != 'devel',
		mysqlRootPw         : mysqlRootPw,
		addGit              : addGit,
		gitRemoteUrl        : gitRemoteUrl,
		develDomain         : develDomain,
		installServerScripts: serverType != 'devel',
		serverType          : serverType,
		localize            : localize,
		locales             : locales,
		standardLocale      : standardLocale,
		dbUser              : dbUser || format( '%s_user', projectName ),
		dbPw                : dbPw || pwGen( 15, false ),
		dbName              : dbName || format( '%s_db', projectName ),
		dbPwFilename        : format( '/etc/webapps/%s-django.dbpw', projectName ),
		secretKeyFilename   : format( '/etc/webapps/%s-django.key', projectName ),
		secretKeyDevel      : pwGen( 50, false ),
		secretKeyProduction : pwGen( 50, false ),
		envPath             : envPath,
		wsgiPath            : serverType == 'apache-wsgi' ? path.join( '/usr', 'local', 'wsgi' ) : 
			path.join( installDir, projectName, projectName ),
		wsgiFilename        : serverType.indexOf( 'daphne' ) != -1 ? format( '%s.asgi', projectName ) : 
			format( '%s.wsgi', projectName ),
		systemUserName      : systemUserName || projectName,
		systemGroupName     : systemGroupName ||'webapps',
		logDir              : path.join( envPath, 'logs' ),
		runDir              : path.join( envPath, 'run' ),
		useExistingProject  : useExistingProject,
		isPython3           : parseInt( pythonVersionNumber.charAt( 0 ) ) == 3,
		djangoVersionNumber : djangoVersionNumber,
		cloneExistingProject: cloneExistingProject,
		serverPort          : serverPort,
		serverPortAsync     : serverPortAsync,
		binAsync            : binAsync,
		domainAsync         : domainAsync,
		entryPointAsync     : entryPointAsync,
		projectDirAsync     : projectDirAsync,
		binNpm              : binAsync ? format( '%s/npm', path.dirname( binAsync ) ) : undefined,
	} )

});});});});});});});});});});});});});});});});});});});});});});});});});});});});});});