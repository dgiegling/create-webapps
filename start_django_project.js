
var format = require( 'util' ).format
var changeBackToDirname = require( './common' ).changeBackToDirname
var path = require( 'path' )

var getCommands = function( args ){
 
	var cmds = []	
	cmds.push( format( 'mkdir -p %s', path.join( args.projectDir, 'static' ) ) )
	cmds.push( format( 'touch %s/robots.txt', path.join( args.projectDir, 'static' ) ) )
	cmds.push( format( 'touch %s/favicon.ico', path.join( args.projectDir, 'static' ) ) )
	cmds.push( format( 'mkdir -p %s', path.join( args.projectDir, 'static', 'css' ) ) )
	cmds.push( format( 'mkdir -p %s', path.join( args.projectDir, 'static', 'js' ) ) )
	cmds.push( format( 'mkdir -p %s', path.join( args.projectDir, args.appName, 'templates' ) ) )
	cmds.push( format( 'mkdir -p %s', path.join( args.projectDir, args.appName, 'templates', args.appName ) ) )
	cmds.push( format( 'mkdir -p %s', path.join( args.projectDir, args.appName, 'templatetags' ) ) )
	cmds.push( format( 'touch %s/__init__.py', path.join( args.projectDir, args.appName, 'templatetags' ) ) )
	cmds.push( format( 'touch %s/costum_tags.py', path.join( args.projectDir, args.appName, 'templatetags' ) ) )

	return cmds
}

var startDjangoProject = function( args, callback ) {
	var execCmd = require( './common' ).execCmd
	var execCmds = require( './common' ).execCmds
	var activate = format( 'source %s/bin/activate', args.envPath )

	process.chdir( args.envPath )
	var cmd = format( '%s && django-admin.py startproject %s', activate, args.projectName )

	execCmd( cmd, function(){
		process.chdir( args.projectDir )

		var cmd = format( '%s && python manage.py startapp %s', activate, args.appName )
		execCmd( cmd, function(){
			execCmds( getCommands( args ), function(){
				
				changeBackToDirname()
				callback && callback()
			
			}, changeBackToDirname )
		
		}, changeBackToDirname )

	}, changeBackToDirname )
}

var startDjangoProjectSync = function( args ) {
	var execCmdSync = require( './common' ).execCmdSync
	var execCmdsSync = require( './common' ).execCmdsSync
	var activate = format( 'source %s/bin/activate', args.envPath )

	// start project
	process.chdir( args.envPath )
	var cmd = format( '%s && django-admin.py startproject %s', activate, args.projectName )
	execCmdSync( cmd )
	
	// start app
	process.chdir( args.projectDir )
	var cmd = format( '%s && python manage.py startapp %s', activate, args.appName )
	execCmdSync( cmd )
	
	// mk dirs and files
	execCmdsSync( getCommands( args ) )

	changeBackToDirname()
}

module.exports = {
	//startDjangoProject    : startDjangoProject,
	startDjangoProjectSync: startDjangoProjectSync
}