var format = require( 'util' ).format
var path = require( 'path' )
var changeBackToDirname = require( './common' ).changeBackToDirname

var commonModules = [
	'express',
	'jinjs',
]

var globalModules = [
	'coffee-script',
	'yuglify',
	'less',
	'bower'
]

var getCommands = function( args ){

	var npm = format( '%s/npm', path.dirname( args.binAsync ) )

	var cmds = [
		format( '%s install %s', npm, commonModules.join( ' ' ) )
	]	
	
	
	// db stuff
	// postgre
	//if( ( !args.isProductionServer && args.databaseBackendDevel == 'postgresql' ) || 
	//	( args.isProductionServer && args.databaseBackendProduction == 'postgresql' ) )
	//	cmds.push( format( '%s && pip install psycopg2',activate ) )
	// mysql
	//else if( ( !args.isProductionServer && args.databaseBackendDevel == 'mysql' ) || 
	//	( args.isProductionServer && args.databaseBackendProduction == 'mysql' ) )
	//	cmds.push( format( '%s && pip install --allow-all-external mysql-connector-python', activate ) )

	if( args.programmingEnv.indexOf( 'django' ) != -1 )
		cmds.push( format( '%s install redis', npm ) )

	return cmds
}

var asyncInstalls = function( args, callback ) {
	var execCmds = require( './common' ).execCmds
	process.chdir( args.envPath )
	execCmds( getCommands( args ), function(){
		changeBackToDirname()
		callback && callback()
	}, changeBackToDirname )
}

var asyncInstallsSync = function( args ) {
	var execCmdsSync = require( './common' ).execCmdsSync
	process.chdir( args.envPath )
	res = execCmdsSync( args, changeBackToDirname )
	changeBackToDirname()
	return res
}

module.exports = {
	asyncInstalls    : asyncInstalls,
	asyncInstallsSync: asyncInstallsSync
}