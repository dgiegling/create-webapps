var format = require( 'util' ).format

var mysqlInit = function( args, callback ) {
	var execCmd = require( './common' ).execCmd
	var cmd = format( 'mysql -u root -p%s -e "CREATE DATABASE %s"', args.mysqlRootPw, args.dbName )
	
	execCmd( cmd, function(  ){
		var cmd = format( 'mysql -u root -p%s -e "GRANT ALL PRIVILEGES ON %s.* TO %s@localhost IDENTIFIED BY \'%s\'"', 
			args.mysqlRootPw, args.dbName, args.dbUser, args.dbPw )
		execCmd( cmd, callback )
	} )
}

var mysqlInitSync = function( args ) {
	var execCmdSync = require( './common' ).execCmdSync
	var cmd = format( 'mysql -u root -p%s -e "CREATE DATABASE %s"', args.mysqlRootPw, args.dbName )
	execCmdSync( cmd )
	
	var cmd = format( 'mysql -u root -p%s -e "GRANT ALL PRIVILEGES ON %s.* TO %s@localhost IDENTIFIED BY \'%s\'"', 
		args.mysqlRootPw, args.dbName, args.dbUser, args.dbPw )
	execCmdSync( cmd )
}

module.exports = {
	mysqlInit    : mysqlInit,
	mysqlInitSync: mysqlInitSync
}
