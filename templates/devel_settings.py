
DEBUG          = True
TEMPLATE_DEBUG = True
ALLOWED_HOSTS  = []
SECRET_KEY     = '{{ args.secretKeyDevel }}'

{% if args.databaseBackendDevel == 'sqllite' %}
import os
BASE_DIR  = os.path.dirname( os.path.dirname( __file__ ) )
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
{% else %}
DATABASES = {
    'default': {
        'ENGINE': '{% if args.databaseBackendDevel == 'mysql' %}mysql.connector.django{% else %}django.db.backends.postgresql_psycopg2{% endif %}',
        'NAME': '{{ args.dbName }}',
        'USER': '{{ args.dbUser }}',
        'PASSWORD': '{{ args.dbPw }}',
        'HOST': '127.0.0.1',
        'PORT': '{% if args.databaseBackendDevel == 'mysql' %}3306{% endif %}',
        'CHARSET': 'utf8',
        'COLLATION': 'utf8_unicode_ci'
    }
}
{% endif %}

COMPRESS = False

{% if args.localize %}
ROSETTA_CACHE_NAME = 'default'
{% endif %}