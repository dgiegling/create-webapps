#!/bin/bash
NP=$NODE_PATH
for d in {{ args.projectDir }}/*/ ; do
	a="async"
	if [ -e $d$a ]
		then
		if [[ $NODE_PATH != *"$d$a"* ]]
			then
			#echo "exporting $d$a"
			export NODE_PATH=$NODE_PATH:$d$a
		fi
	fi
done

{{ args.binAsync }} {{ args.projectDirAsync }}/{{ args.entryPointAsync }}