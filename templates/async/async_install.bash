#!/bin/bash
cur=$(pwd)
cd {{ args.envPath }}
{{ args.binNpm }} install $1
cd $cur