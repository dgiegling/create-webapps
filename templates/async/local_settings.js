{% if args.isProductionServer %}
module.exports = require( './production_settings' )
module.exports.PORT = '{{ args.runDir }}/async.sock',
{% else %}
module.exports = require( './devel_settings' )
module.exports.PORT = {{ args.serverPortAsync }}
{% endif %}