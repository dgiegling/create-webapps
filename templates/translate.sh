source {{ args.envPath }}/bin/activate
cd {{ args.appName }}
django-admin.py makemessages -a
django-admin.py compilemessages
cd ..