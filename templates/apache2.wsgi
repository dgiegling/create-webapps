import os, sys

paths = [ 
	'{{ args.projectDir }}',
	os.path.join( '{{ args.projectDir }}', '{{ args.projectName }}' ),
	os.path.join( '{{ args.projectDir }}', '{{ args.appName }}' ), ]
for path in paths:
	if path not in sys.path:
		sys.path.append( path )
os.environ['DJANGO_SETTINGS_MODULE'] = '{{ args.projectName }}.settings'

import django.core.handlers.wsgi

application = django.core.handlers.wsgi.WSGIHandler()