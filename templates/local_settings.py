{% if args.isProductionServer %}
from {% if args.isPython3 %}.{% endif %}production_settings import *
{% else %}
from {% if args.isPython3 %}.{% endif %}devel_settings import *
{% endif %}