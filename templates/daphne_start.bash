#!/bin/bash

NAME={{ args.projectName }}_django_app                  # Name of the application
DJANGODIR={{ args.projectDir }}            # Django project directory
SOCKFILE={{ args.runDir}}/daphne.sock  # we will communicte using this unix socket
USER={{ args.systemUserName }}                                        # the user to run as
GROUP={{ args.systemGroupName }}                                   # the group to run as
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE={{ args.projectName }}.settings             # which settings file should Django use
DJANGO_WSGI_MODULE={{ args.wsgiFilename }}                    # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source {{ args.envPath }}/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django daphne
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec {{ args.envPath }}/bin/daphne ${DJANGO_WSGI_MODULE}:application -u $SOCKFILE