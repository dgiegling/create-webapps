source {{ args.envPath }}/bin/activate
git pull
python manage.py collectstatic
# code reload
{% if args.serverType == "apache-wsgi" %}apache2ctl restart{% elif args.serverType == "nginx-gunicorn" %}service nginx restart{% endif %}