# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
{% if args.databaseBackendProduction == 'sqllite' %}
import os
BASE_DIR  = os.path.dirname( os.path.dirname( __file__ ) )
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join( BASE_DIR, 'db.sqlite3' ),
    }
}
{% else %}
with open( '{{ args.dbPwFilename }}' ) as f:
    DB_PASSWORD = f.read().strip()
DATABASES = {
    'default': {
        'ENGINE': '{% if args.databaseBackendProduction == 'mysql' %}mysql.connector.django{% else %}django.db.backends.postgresql_psycopg2{% endif %}',
        'NAME': '{{ args.dbName }}',
        'USER': '{{ args.dbUser }}',
        'PASSWORD': DB_PASSWORD,
        'HOST': '127.0.0.1',
        'PORT': '{% if args.databaseBackendProduction == 'mysql' %}3306{% endif %}',
        'CHARSET': 'utf8',
        'COLLATION': 'utf8_unicode_ci'
    }
}
{% endif %}

DEBUG          = False
TEMPLATE_DEBUG = False
COMPRESS       = True

ADMIN_MEDIA_PREFIX = "admin/"

ALLOWED_HOSTS = [ 
    '{{ args.domain }}', 
    '{{ args.domain }}.', 
    '*.{{ args.domain }}', 
    '*.{{ args.domain }}.', 
    {% if args.develDomain != '' %}
    '{{ args.develDomain }}', 
    '{{ args.develDomain }}.', 
    '*.{{ args.develDomain }}', 
    '*.{{ args.develDomain }}.', 
    {% endif %}]
ALLOWED_INCLUDE_ROOTS = ('/var/www',) # TODO -> better check with static root and media root

with open( '{{ args.secretKeyFilename }}' ) as f:
    SECRET_KEY = f.read().strip()

'''
TEMPLATE_LOADERS = (
    ('django_mobile.loader.CachedLoader', (
        'django_mobile.loader.Loader',
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.i18n',
    'django.contrib.auth.context_processors.auth',
    "django_mobile.context_processors.is_mobile",
    'django_mobile.context_processors.flavour',
)
'''

#SSL_ALWAYS = False