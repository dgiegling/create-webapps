from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
{% if args.localize %}
from django.conf.urls.i18n import i18n_patterns
{% endif %}
#admin.autodiscover()

urlpatterns = patterns( '',
    url( r'^admin/', include( admin.site.urls ) ),
    {% if args.localize %}
    url( '', include( '{{ args.appName }}.urls' ) ),
    {% else %}
    url( r'^translation/', include('rosetta.urls') ),
    {% endif %}
) + static( settings.MEDIA_URL, document_root=settings.MEDIA_ROOT )

{% if args.localize %}
urlpatterns += i18n_patterns( '', url( r'^', include( '{{ args.appName }}.urls' ) ), )
{% endif %}