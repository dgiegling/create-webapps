"""
Django settings for {{ args.projectName }} project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/


ROOT_URLCONF       = '{{ args.projectName }}.urls'
WSGI_APPLICATION   = '{{ args.projectName }}.wsgi.application'
LOGIN_URL          = '/backend/login'
LOGIN_REDIRECT_URL = '/backend/'


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

TIME_ZONE = 'UTC'
USE_I18N = {% if args.localize %}True{% else %}False{% endif %}
USE_L10N = {% if args.localize %}True{% else %}False{% endif %}
USE_TZ = True

{% if args.localize %}
LANGUAGES = (
    {% for lang in args.locales %}
    ( '{{ lang }}', '{{ lang }}-name' ),
    {% endfor %}
)
LANGUAGE_CODE                       = '{{ args.standardLocale }}'
MODELTRANSLATION_DEFAULT_LANGUAGE   = '{{ args.standardLocale }}'
#MODELTRANSLATION_FALLBACK_LANGUAGES = ( '{{ args.standardLocale }}', )
LANGUAGE_COOKIE_NAME = 'user_language'
LOCALE_PATHS = ( "/{{ args.appName }}/locale/", )
ROSETTA_STORAGE_CLASS = 'rosetta.storage.CacheRosettaStorage'
{% endif %}


INSTALLED_APPS = (
    {% if args.localize %}'modeltranslation',{% endif %}
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'widget_tweaks',
    {% if args.localize %}'rosetta',{% endif %}
    '{{ args.appName }}',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'ssl_redirect.middleware.SSLRedirectMiddleware',
    #'django_mobile.middleware.MobileDetectionMiddleware',
    #'django_mobile.middleware.SetFlavourMiddleware',
)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder'
)
STATIC_ROOT      = '{{ args.staticRoot }}'
MEDIA_ROOT       = '{{ args.mediaRoot }}'
MEDIA_URL        = '/media/'
STATIC_URL       = '/static/'
STATICFILES_DIRS = ( os.path.join( BASE_DIR, 'static' ),)
TEMPLATE_DIRS    = [ os.path.join( BASE_DIR, 'templates' )]

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'unique-snowflake'
    }
}

SESSION_ENGINE = "django.contrib.sessions.backends.signed_cookies"

#try:

from {% if args.isPython3 %}.{% endif %}local_settings import *