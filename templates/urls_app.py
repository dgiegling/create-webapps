from django.conf.urls import patterns, url
from django.conf import settings
from . import views
#from . import backend

urlpatterns = patterns('',


	# backend
	#url(r'^backend/login/$', backend.login, { 'template_name': '{{ args.appName }}/login.html' }, 
	#	name="login" ),
	
	#url(r'^backend/logout/$','django.contrib.auth.views.logout_then_login', name="logout"),
	
	#url(r'^backend/changepassword/$','django.contrib.auth.views.password_change', 
	#	{'template_name': '{{ args.appName }}/password-change.html'}, name="changePassword"),
	
	#url(r'^backend/changepassworddone/$','django.contrib.auth.views.password_change_done', 
	#	{'template_name': '{{ args.appName }}/password-change.html'}, name="password_change_done"),
	
	#url( r'^backend$', backend.backendIndex, name='backendIndex' ),
	#url( r'^backend/page/(?P<pageName>[a-zA-Z0-9\-_]+)/$', backend.backendIndex, name="backendPage" ),
	#url( r'^backend/newitem/(?P<pageName>[a-zA-Z0-9\-_]+)/$', backend.backendForm, name="backendNewItem" ),
	#url( r'^backend/item/(?P<pageName>[a-zA-Z0-9\-_]+)/(?P<itemId>[0-9]+)$', backend.backendForm, name="backendItem" ),
	#url( r'^backend/delete/(?P<pageName>[a-zA-Z0-9\-_]+)/(?P<instanceId>[0-9]+)$', backend.delete, name="delete" ),

	# frontend
	#url( r'^$', views.page, name='home' ),
	#url( r'^(?P<pageName>[a-zA-Z0-9\-_]+)$', views.page, name='page'),
	#url( r'^(?P<pageName>[a-zA-Z0-9\-_]+)/(?P<itemId>[0-9]+)$', views.page, name='item'),
)

if settings.DEBUG:
	urlpatterns += patterns('django.contrib.staticfiles.views',
		url(r'^static/(?P<path>.*)$', 'serve'),
)