var format = require( 'util' ).format

var commonPythonModules = [
	'django-widget-tweaks',
	'django-pipeline',
	'manifesto'
]

var getCommands = function( args ){

	var activate = format( 'source %s/bin/activate', args.envPath )

	// setup virtual env
	// common packages
	var cmds = [
		format( '%s && pip install %s', activate, commonPythonModules.join( ' ' ) ),
	]	

	cmds.push( format( '%s && pip install -r %s/%s/requirements.txt', activate, args.envPath, args.projectName ) )
	
	// django
	if( args.djangoVersionNumber == '' )
		cmds.push( format( '%s && pip install django', activate ) )
	else
		cmds.push( format( '%s && pip install django==%s', activate, args.djangoVersionNumber ) )

	// internationilizaton
	if( args.localize )
		cmds.push( format( '%s && pip install django-rosetta django-modeltranslation', activate ) )
	
	// gunicorn
	if( args.serverType.indexOf( 'gunicorn' ) != -1 )
		cmds.push( format( '%s && pip install gunicorn setproctitle', activate ) )

	else if( args.serverType.indexOf( 'daphne' ) != -1 )
		cmds.push( format( '%s && pip install daphne', activate ) )

	// db stuff
	// postgre
	if( ( !args.isProductionServer && args.databaseBackendDevel == 'postgresql' ) || 
		( args.isProductionServer && args.databaseBackendProduction == 'postgresql' ) )
		cmds.push( format( '%s && pip install psycopg2',activate ) )
	// mysql
	else if( ( !args.isProductionServer && args.databaseBackendDevel == 'mysql' ) || 
		( args.isProductionServer && args.databaseBackendProduction == 'mysql' ) )
		cmds.push( format( '%s && pip install --allow-all-external mysql-connector-python', activate ) )

	// south for django < 1.7
	if( args.djangoVersionNumber.length > 2 && parseInt( args.djangoVersionNumber.charAt( 2 ) ) < 7 )
		cmds.push( format( '%s && pip install south', activate ) )

	if( args.programmingEnv.indexOf( 'async' ) != -1 )
		cmds.push( format( '%s && pip install redis', activate ) )

	return cmds
}

/*
var djangoInstalls = function( args, callback ) {
	var cmd = format( 'virtualenv -p /usr/bin/python%s %s', args.pythonVersionNumber, args.envPath )
	require( './common' ).execCmd( cmd, function(){
		require( './common' ).execCmds( getCommands( args ), callback, function( err ){
			console.log( 'error in django installs' )
			console.log( err )
		} )
	} )
}
*/

var djangoInstallsSync = function( args ) {
	//var cmd = format( 'virtualenv -p /usr/bin/python%s %s', args.pythonVersionNumber, args.envPath )
	//require( './common' ).execCmdSync( cmd )
	var cmd = format( 'pyvenv %s', args.envPath )
	require( './common' ).execCmdSync( cmd )
	return require( './common' ).execCmdsSync( getCommands( args ) )
}

var djangoInstallsSync34 = function( args ) {
	var cmd = format( 'virtualenv -p /usr/bin/python%s %s', args.pythonVersionNumber, args.envPath )
	require( './common' ).execCmdSync( cmd )
	//var cmd = format( 'pyvenv %s', args.envPath )
	//require( './common' ).execCmdSync( cmd )
	return require( './common' ).execCmdsSync( getCommands( args ) )
}

var djangoInstallsSync36 = function( args ){
	var cmd = format( 'python3 -m venv %s', args.envPath )
	require( './common' ).execCmdSync( cmd )
	return require( './common' ).execCmdsSync( getCommands( args ) )
}

module.exports = {
	//djangoInstalls    : djangoInstalls,
	djangoInstallsSync: djangoInstallsSync,
	djangoInstallsSync34: djangoInstallsSync34,
	djangoInstallsSync36: djangoInstallsSync36,
}