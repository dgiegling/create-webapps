var format = require( 'util' ).format
var suppose = require( './suppose/suppose' )

var postgresInit = function( args, callback ) {
	var execCmd = require( './common' ).execCmd
	//var cmd = format( 'su - postgres -c "createuser -P %s"', args.dbUser )

	var hadError = false
	suppose( 'scripts/postgres_init.sh', [ args.dbName, args.dbUser ] )
		.on( "Enter password for new role: " ).respond( format( '%s\n', args.dbPw ) )
		.on( "Enter it again: " ).respond( format( '%s\n', args.dbPw ) )
		.on( "Shall the new role be a superuser? (y/n)" ).respond( 'n' )
	.error( function( err ){
		if( err.message.trim() != '' ){
			console.log( err )
			if( err.toString().indexOf( 'already exists' ) == -1 )
				hadError = true

		}
	} )
	.end( function( code ){
		if( hadError ){
			console.log( 'error in db init! giving up!' )
			return
		}
		console.log( format( 'added postgresql db "%s"', args.dbName ) )
		console.log( format( 'added postgresql user "%s"', args.dbUser ) )
		console.log( format( 'pw is %s', args.dbPw ) )
		callback && callback()

		/*var cmd = format( 'su - postgres -c "createdb --owner %s %s"', args.dbUser, args.dbName )
		execCmd( cmd, function(){

			console.log( format( 'added postgresql db "%s"', args.dbName ) )
			console.log( format( 'added postgresql user "%s"', args.dbUser ) )
			console.log( format( 'pw is %s', args.dbPw ) )
			callback && callback()
			
		} )*/
	} )
}


module.exports = {
	postgresInit    : postgresInit
}